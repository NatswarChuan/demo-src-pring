# Giai đoạn build
FROM gradle:7.6.1-jdk17 AS TEMP_BUILD_IMAGE

WORKDIR /app
COPY . /app

RUN gradle build

EXPOSE 8080
CMD ["gradle", "bootRun"]