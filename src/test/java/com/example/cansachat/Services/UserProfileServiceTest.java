package com.example.cansachat.Services;

import com.example.cansachat.Entities.Profile;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.ExceptionMessage.ProfileExceptionMessage;
import com.example.cansachat.Dtos.Requests.ProfileCreateRequestDto;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IUserProfileService;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * UserProfileServiceTest là một lớp kiểm thử đơn vị (unit test) cho UseProfilerService.
 * Nó sử dụng framework Mockito và Spring Boot Test để kiểm tra các phương thức
 * trong UseProfilerService.
 */
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserProfileServiceTest {

    @Autowired
    private IUserProfileService useProfilerService;

    /**
     * 
     * Kiểm tra tài khoản đã có thông tin.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCreateProfileRegistedProfile() throws HttpException {
        // Chuẩn bị dữ liệu giả định
        User user = new User();
        Profile profile = new Profile();
        user.setProfile(profile);
        ProfileCreateRequestDto createRequesrDto = new ProfileCreateRequestDto();

        // Gọi phương thức createProfile và kiểm tra ngoại lệ
        assertThrows(HttpException.class, () -> useProfilerService.createProfile(createRequesrDto, user),
                ProfileExceptionMessage.REGISTERED);
    }
}
