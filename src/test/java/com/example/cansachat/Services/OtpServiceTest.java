package com.example.cansachat.Services;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.UserStatus;
import com.example.cansachat.Commond.ExceptionMessage.OtpExceptionMessage;
import com.example.cansachat.Commond.ExceptionMessage.UserExceptionMessage;
import com.example.cansachat.Entities.Otp;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IOtpService;
import com.example.cansachat.Repositories.OtpRepository;
import com.example.cansachat.Repositories.UserRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * 
 * Bộ kiểm thử cho lớp OtpService.
 */
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class OtpServiceTest {

    @Autowired
    private IOtpService otpService;

    @MockBean
    private OtpRepository otpRepository;

    @MockBean
    private UserRepository userRepository;

    /**
     * 
     * Kiểm tra khi email không hợp lệ.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpLoginInvalidEmail() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.empty());

        assertThrows(HttpException.class, () -> otpService.checkOtpLogin(email, otpCode),
                UserExceptionMessage.UNREGISTER);
    }

    /**
     * 
     * Kiểm tra khi tài khoản chưa kích hoạt.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpLoginInactiveUser() throws HttpException {
        String otpCode = "123456";
        String email = "example@example.com";
        Otp otp = new Otp();
        User user = new User();
        user.setPrivacyStatus(UserStatus.INACTIVE);
        otp.setUser(user);

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpLogin(email, otpCode),
                UserExceptionMessage.INACTIVE);
    }

    /**
     * 
     * Kiểm tra khi mã OTP không hợp lệ.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpLoginInvalidOTP() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";
        String otpCodeFail = "123457";
        Otp otp = new Otp();
        otp.setOtp(otpCode);
        User user = new User();
        user.setPrivacyStatus(UserStatus.ACTIVE);
        otp.setUser(user);

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpLogin(email, otpCodeFail),
                OtpExceptionMessage.OTP_INVALID);
    }

    /**
     * 
     * Kiểm tra khi mã OTP đã hết hiệu lực.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpLoginExpiredOtp() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";
        Otp otp = new Otp();
        otp.setOtp(otpCode);
        User user = new User();
        user.setPrivacyStatus(UserStatus.ACTIVE);
        otp.setUser(user);
        otp.setUpdatedAt(LocalDateTime.now().minusMinutes(10));

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpLogin(email, otpCode),
                OtpExceptionMessage.OTP_EXPIRED);
    }

    /**
     * 
     * Kiểm tra khi email đã được đăng ký.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpRegisterInvalidEmail() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.empty());

        assertThrows(HttpException.class, () -> otpService.checkOtpRegister(email, otpCode),
                UserExceptionMessage.UNREGISTER);
        verify(userRepository, never()).save(any(User.class));
    }

    /**
     * 
     * Kiểm tra khi tài khoản đã được đăng ký và kích hoạt.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpRegisterRegisteredUser() throws HttpException {
        String otpCode = "123456";
        String email = "example@example.com";
        Otp otp = new Otp();
        User user = new User();
        user.setPrivacyStatus(UserStatus.ACTIVE);
        otp.setUser(user);

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpRegister(email, otpCode),
                UserExceptionMessage.REGISTERED);
        verify(userRepository, never()).save(any(User.class));

    }

    /**
     * 
     * Kiểm tra khi mã OTP không hợp lệ trong quá trình đăng ký.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */
    @Test
    public void testCheckOtpRegisterInvalidOTP() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";
        String otpCodeFail = "123457";
        Otp otp = new Otp();
        otp.setOtp(otpCode);
        User user = new User();
        user.setPrivacyStatus(UserStatus.INACTIVE);
        otp.setUser(user);

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpRegister(email, otpCodeFail),
                OtpExceptionMessage.OTP_INVALID);
        verify(userRepository, never()).save(any(User.class));
    }

    /**
     * 
     * Kiểm tra khi mã OTP đã hết hiệu lực trong quá trình đăng ký.
     * 
     * @throws HttpException nếu xảy ra lỗi HttpException.
     */

    @Test
    public void testCheckOtpRegisterExpiredOtp() throws HttpException {
        String email = "example@example.com";
        String otpCode = "123456";
        Otp otp = new Otp();
        otp.setOtp(otpCode);
        User user = new User();
        user.setPrivacyStatus(UserStatus.INACTIVE);
        otp.setUser(user);
        otp.setUpdatedAt(LocalDateTime.now().minusMinutes(10));

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        assertThrows(HttpException.class, () -> otpService.checkOtpRegister(email, otpCode),
                OtpExceptionMessage.OTP_EXPIRED);
        verify(userRepository, never()).save(any(User.class));
    }
}
