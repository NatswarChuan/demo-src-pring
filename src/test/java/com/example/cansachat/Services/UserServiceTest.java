package com.example.cansachat.Services;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.UserStatus;
import com.example.cansachat.Commond.ExceptionMessage.UserExceptionMessage;
import com.example.cansachat.Entities.Otp;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IUserService;
import com.example.cansachat.Repositories.OtpRepository;
import com.example.cansachat.Repositories.UserRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * UserServiceTest là một lớp kiểm thử đơn vị (unit test) cho UserService.
 * Nó sử dụng framework Mockito và Spring Boot Test để kiểm tra các phương thức
 * trong UserService.
 */
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @MockBean
    private OtpRepository otpRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private MailService mailService;

    /**
     * Phương thức kiểm tra trường hợp đăng nhập với email không hợp lệ.
     * 
     * @throws HttpException
     */
    @Test
    public void testLoginInvalidEmail() throws HttpException {
        // Chuẩn bị dữ liệu giả định
        String email = "example@example.com";

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.empty());

        // Gọi phương thức login và kiểm tra ngoại lệ
        assertThrows(HttpException.class, () -> userService.login(email), UserExceptionMessage.UNREGISTER);

        // Kiểm tra rằng mailService.send() không được gọi
        verify(mailService, never()).send(anyString(), anyString(), anyString());
        // Kiểm tra rằng otpRepository.save() không được gọi
        verify(otpRepository, never()).save(any(Otp.class));
    }

    /**
     * Phương thức kiểm tra trường hợp đăng nhập với email không hợp lệ.
     * 
     * @throws HttpException
     */
    @Test
    public void testLoginInactiveUser() throws HttpException {
        // Chuẩn bị dữ liệu giả định
        String email = "example@example.com";
        Otp otp = new Otp();
        User user = new User();
        user.setPrivacyStatus(UserStatus.INACTIVE);
        otp.setUser(user);

        when(otpRepository.findByUser_Email(email)).thenReturn(Optional.of(otp));

        // Gọi phương thức login và kiểm tra ngoại lệ
        assertThrows(HttpException.class, () -> userService.login(email), UserExceptionMessage.INACTIVE);

        // Kiểm tra rằng mailService.send() không được gọi
        verify(mailService, never()).send(anyString(), anyString(), anyString());
        // Kiểm tra rằng otpRepository.save() không được gọi
        verify(otpRepository, never()).save(any(Otp.class));
    }

    @Test
    public void testRegisterRegistedUser() throws HttpException {
        // Chuẩn bị dữ liệu giả định
        String email = "example@example.com";
        User user = new User();
        user.setEmail(email);

        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        // Gọi phương thức login và kiểm tra ngoại lệ
        assertThrows(HttpException.class, () -> userService.login(email), UserExceptionMessage.REGISTERED);

        // Kiểm tra rằng mailService.send() không được gọi
        verify(mailService, never()).send(anyString(), anyString(), anyString());
        // Kiểm tra rằng otpRepository.save() không được gọi
        verify(otpRepository, never()).save(any(Otp.class));
    }
}
