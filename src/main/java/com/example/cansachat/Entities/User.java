package com.example.cansachat.Entities;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import lombok.*;

/**
 * Đây là một đối tượng Entity đại diện cho thông tin người dùng.
 */
@Data
@Entity
@Table(name = "csc_users", schema = "chat_cansa")
public class User implements Serializable {
    /**
     * Id duy nhất của người dùng.
     */
    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID userId;

    /**
     * Địa chỉ email của người dùng.
     */
    @Column(name = "email")
    private String email;

    /**
     * Tag của người dùng.
     */
    @Column(name = "tag")
    private String tag;

    /**
     * Thời điểm tạo người dùng.
     */
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    /**
     * Thời điểm cập nhật thông tin người dùng lần cuối.
     */
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt = LocalDateTime.now();

    /**
     * Trạng thái quyền riêng tư của người dùng.
     */
    @Column(name = "privacy_status")
    private int privacyStatus = 0;

    /**
     * Mối quan hệ một-một với đối tượng Otp, được map thông qua thuộc tính "user".
     * Cascade xóa được áp dụng khi người dùng bị xóa.
     */
    @OneToOne(mappedBy = "user", targetEntity = Otp.class, cascade = CascadeType.REMOVE)
    private Otp otp;

    @OneToOne(mappedBy = "user", targetEntity = Profile.class, cascade = CascadeType.ALL)
    private Profile profile;
}