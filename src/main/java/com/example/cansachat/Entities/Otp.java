package com.example.cansachat.Entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Đây là một Entity đại diện cho OTP (One-Time Password) trong hệ thống.
 * OTP được sử dụng để xác thực người dùng trong quá trình đăng nhập hoặc thực
 * hiện các hành động quan trọng.
 */
@Entity
@Table(name = "csc_otps", schema = "chat_cansa")
@Data
public class Otp implements Serializable {

    /**
     * Id duy nhất của OTP.
     */
    @Id
    @Column(name = "otp_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID otpId;

    /**
     * Giá trị OTP.
     */
    @Column(name = "otp")
    private String otp;

    /**
     * Thời điểm OTP được tạo ra.
     */
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    /**
     * Thời điểm cập nhật OTP gần nhất.
     */
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt = LocalDateTime.now();

    /**
     * Người dùng liên kết với OTP.
     */
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
