package com.example.cansachat.Repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.cansachat.Entities.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile,UUID > {
    Optional<Profile> findByUser_UserId(UUID userId);
}
