package com.example.cansachat.Dtos.Responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Đại diện cho phản hồi Access Token.
 * 
 * @Data: Tạo các phương thức getter, setter, equals, hashCode và toString tự động.
 * @AllArgsConstructor: Tạo một constructor có tham số cho tất cả các trường.
 */
@Data
@AllArgsConstructor
public class AccessTokenResponse {
    /**
     * Access Token.
     * 
     * @JsonProperty("access-token"): Ánh xạ thuộc tính này với tên "access-token" trong JSON khi serialize/deserialize.
     */
    @JsonProperty("access-token")
    String accessToken;
}
