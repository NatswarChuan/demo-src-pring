package com.example.cansachat.Dtos.Responses;

import java.util.UUID;

import org.springframework.beans.BeanUtils;

import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.IDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Đối tượng DTO (Data Transfer Object) biểu diễn thông tin người dùng trong
 * phản hồi.
 * Implement interface IDto và được sử dụng để chuyển đổi giữa đối tượng người
 * dùng và DTO.
 */
@Data
public class UserResponseDto implements IDto<User> {

    @JsonProperty("id")
    private UUID userId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("privacy-status")
    private int privacyStatus;

    /**
     * Phương thức chuyển đổi đối tượng UserResponseDto thành đối tượng User.
     *
     * @return Đối tượng User tương ứng với UserResponseDto.
     * @throws UnsupportedOperationException Nếu phương thức không được triển khai.
     */
    @Override
    public User toEntity() {
        throw new UnsupportedOperationException("Unimplemented method 'toEntity'");
    }

    /**
     * Phương thức chuyển đổi đối tượng User thành UserResponseDto.
     *
     * @param entity Đối tượng User cần chuyển đổi.
     */
    @Override
    public void toDto(User entity) {
        BeanUtils.copyProperties(entity, this, "createdAt", "updatedAt");
    }
}
