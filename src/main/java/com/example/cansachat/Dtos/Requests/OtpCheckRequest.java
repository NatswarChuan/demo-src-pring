package com.example.cansachat.Dtos.Requests;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

/**
 * Đối tượng đại diện cho yêu cầu kiểm tra mã OTP.
 */
@Data
public class OtpCheckRequest {
    /**
     * Email cần kiểm tra OTP.
     */
    @NotEmpty(message = "Email không được để trống")
    @Email(message = "Địa chỉ email không hợp lệ")
    @JsonProperty("email")
    String email;

    /**
     * Mã OTP cần kiểm tra.
     */
    @NotEmpty(message = "Otp không được để trống")
    @Length(max = 6, min = 6, message = "Otp phải có 6 chữ số")
    @JsonProperty("otp")
    String otp;
}
