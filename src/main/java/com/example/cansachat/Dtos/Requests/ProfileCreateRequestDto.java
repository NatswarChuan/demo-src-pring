package com.example.cansachat.Dtos.Requests;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.example.cansachat.Commond.ProflleConst;
import com.example.cansachat.Entities.Profile;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.IDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class ProfileCreateRequestDto implements IDto<Profile> {

    @NotEmpty(message = "Tên không được để trống")
    @JsonProperty("full-name")
    private String fullName;

    @JsonProperty("about")
    private String about = null;

    @JsonProperty("tag")
    private String tag = null;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("birthday")
    private LocalDate birthday;

    @NotEmpty(message = "Số điện thoại không được để trống")
    @JsonProperty("phone")
    private String phone;

    @JsonIgnore
    private String avatar;

    @JsonIgnore
    private User user;

    @Override
    public Profile toEntity() {
        Profile profile = new Profile();
        if (this.tag != null) {
            this.user.setTag(tag);
        }
        this.avatar = ProflleConst.AVATAR_DEFAULT;
        BeanUtils.copyProperties(this, profile, "profileId", "updatedAt", "createdAt");
        return profile;
    }

    @Override
    public void toDto(Profile entity) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'toDto'");
    }

}
