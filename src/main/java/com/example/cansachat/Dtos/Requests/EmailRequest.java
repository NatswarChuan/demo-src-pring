package com.example.cansachat.Dtos.Requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Email;
import lombok.Data;

/**
 * Đại diện cho một yêu cầu email.
 *
 * @since 1.0
 */
@Data
public class EmailRequest {

    /**
     * Địa chỉ email.
     *
     * @since 1.0
     */
    @NotEmpty(message = "Email không được để trống")
    @Email(message = "Địa chỉ email không hợp lệ")
    @JsonProperty("email")
    String email;
}
