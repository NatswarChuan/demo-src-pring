package com.example.cansachat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class CansachatApplication {

	public static void main(String[] args) {
		SpringApplication.run(CansachatApplication.class, args);
	}

}
