package com.example.cansachat.Services;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.UserStatus;
import com.example.cansachat.Commond.ExceptionMessage.UserExceptionMessage;
import com.example.cansachat.Entities.Otp;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IUserService;
import com.example.cansachat.Repositories.OtpRepository;
import com.example.cansachat.Repositories.UserRepository;

import jakarta.transaction.Transactional;

/**
 * Dịch vụ UserService thực hiện các hoạt động liên quan đến người dùng.
 */
@Service
@Transactional
public class UserService extends AbService<Otp, UUID> implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    OtpRepository otpRepository;

    @Autowired
    MailService mailService;

    @Autowired
    LogService logService;

    /**
     * Phương thức đăng nhập người dùng bằng email.
     *
     * @param email Địa chỉ email của người dùng.
     * @throws HttpException Ngoại lệ được ném ra nếu email chưa được đăng ký.
     */
    @Override
    public void login(String email) throws HttpException {
        Optional<Otp> otnOtp = otpRepository.findByUser_Email(email);

        if (otnOtp.isEmpty()) {
            HttpException httpException = new HttpException(HttpStatus.UNAUTHORIZED, UserExceptionMessage.UNREGISTER);
            throw httpException;
        }

        Otp otp = otnOtp.get();

        if (otp.getUser().getPrivacyStatus() == UserStatus.INACTIVE) {
            HttpException httpException = new HttpException(HttpStatus.UNAUTHORIZED, UserExceptionMessage.INACTIVE);
            throw httpException;
        }

        String otpCode = this.generateOtpCode();

        mailService.send(email, "Mã otp đăng nhập", otpCode);

        otp.setOtp(otpCode);
        otp.setUpdatedAt(LocalDateTime.now());
        save(otp);
    }

    /**
     * Phương thức sinh mã OTP ngẫu nhiên.
     *
     * @return Mã OTP được sinh ra.
     */
    public String generateOtpCode() {
        int otpLength = 6;
        String digits = "0123456789";
        Random random = new Random();
        StringBuilder otpCode = new StringBuilder(otpLength);

        for (int i = 0; i < otpLength; i++) {
            int index = random.nextInt(digits.length());
            otpCode.append(digits.charAt(index));
        }

        return otpCode.toString();
    }

    /**
     * Đăng ký người dùng với địa chỉ email.
     * Nếu người dùng đã tồn tại và đang hoạt động, sẽ ném ra ngoại lệ
     * HttpException.
     * Nếu người dùng chưa tồn tại, sẽ tạo người dùng mới và gửi mã OTP kích hoạt đến địa chỉ email.
     *
     * @param email Địa chỉ email của người dùng để đăng ký
     * @throws HttpException Ngoại lệ HttpException được ném ra nếu có lỗi xảy ra
     *                       trong quá trình đăng ký
     */
    @Override
    public void register(String email) throws HttpException {
        Optional<User> otnUser = userRepository.findByEmail(email);
        String otpCode;
        User user;
        Otp otp;

        if (otnUser.isPresent()) {
            user = otnUser.get();
            if (user.getPrivacyStatus() == UserStatus.ACTIVE) {
                throw new HttpException(HttpStatus.BAD_REQUEST, UserExceptionMessage.REGISTERED);
            }
            otp = user.getOtp();
        } else {
            user = new User();
            otp = new Otp();
            String[] emailSplit = email.split("@");
            user.setEmail(email);
            user.setTag(emailSplit[0]);
            otp.setUser(user);
        }

        otpCode = this.generateOtpCode();

        mailService.send(email, "Mã otp kích hoạt", otpCode);

        otp.setOtp(otpCode);
        userRepository.save(user);
        save(otp);
    }
}
