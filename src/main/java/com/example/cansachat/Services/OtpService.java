package com.example.cansachat.Services;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.UserStatus;
import com.example.cansachat.Commond.ExceptionMessage.OtpExceptionMessage;
import com.example.cansachat.Commond.ExceptionMessage.UserExceptionMessage;
import com.example.cansachat.Entities.Otp;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IOtpService;
import com.example.cansachat.Repositories.OtpRepository;
import com.example.cansachat.Repositories.UserRepository;

import jakarta.transaction.Transactional;

/**
 * Dịch vụ xử lý OTP.
 */
@Service
@Transactional
public class OtpService extends AbService<Otp, UUID> implements IOtpService {

    @Autowired
    OtpRepository otpRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${otp.validtime}")
    int validTime;

    /**
     * Kiểm tra OTP.
     *
     * @param email   Email người dùng
     * @param otpCode Mã OTP
     * @return Người dùng liên kết với OTP hợp lệ
     * @throws HttpException nếu xảy ra lỗi HTTP
     */
    @Override
    public User checkOtpLogin(String email, String otpCode) throws HttpException {
        Optional<Otp> opOtp = otpRepository.findByUser_Email(email);

        if (opOtp.isEmpty()) {
            throw new HttpException(HttpStatus.BAD_REQUEST, UserExceptionMessage.UNREGISTER);
        }

        Otp otp = opOtp.get();

        if (otp.getUser().getPrivacyStatus() == UserStatus.INACTIVE) {
            throw new HttpException(HttpStatus.BAD_REQUEST, UserExceptionMessage.INACTIVE);
        }

        String otpDbCode = otp.getOtp();

        if (!otpDbCode.equals(otpCode)) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_INVALID);
        }

        LocalDateTime validDateTime = otp.getUpdatedAt().plusMinutes(validTime);

        if (validDateTime.compareTo(LocalDateTime.now()) < 0) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_EXPIRED);
        }

        return otp.getUser();
    }

    /**
     * 
     * Kiểm tra mã OTP cho quá trình đăng ký người dùng.
     * 
     * @param email   Địa chỉ email của người dùng.
     * 
     * @param otpCode Mã OTP cần kiểm tra.
     * 
     * @return Người dùng tương ứng với mã OTP đã kiểm tra.
     * 
     * @throws HttpException Nếu xảy ra lỗi trong quá trình kiểm tra OTP.
     */
    @Override
    public User checkOtpRegister(String email, String otpCode) throws HttpException {
        Optional<Otp> opOtp = otpRepository.findByUser_Email(email);

        if (opOtp.isEmpty()) {
            throw new HttpException(HttpStatus.BAD_REQUEST, UserExceptionMessage.UNREGISTER);
        }

        Otp otp = opOtp.get();
        User user = otp.getUser();
        if (user.getPrivacyStatus() == UserStatus.ACTIVE) {
            throw new HttpException(HttpStatus.BAD_REQUEST, UserExceptionMessage.REGISTERED);
        }

        String otpDbCode = otp.getOtp();

        if (!otpDbCode.equals(otpCode)) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_INVALID);
        }

        LocalDateTime otpUpdatedAt = otp.getUpdatedAt();
        LocalDateTime validDateTime = otpUpdatedAt.plusMinutes(validTime);

        if (validDateTime.compareTo(LocalDateTime.now()) < 0) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_EXPIRED);
        }

        user.setPrivacyStatus(UserStatus.ACTIVE);

        userRepository.save(user);
        return otp.getUser();
    }
}
