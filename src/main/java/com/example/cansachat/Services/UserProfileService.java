package com.example.cansachat.Services;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.ExceptionMessage.ProfileExceptionMessage;
import com.example.cansachat.Dtos.Requests.ProfileCreateRequestDto;
import com.example.cansachat.Entities.Profile;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IUserProfileService;
import com.example.cansachat.Repositories.ProfileRepository;

import jakarta.transaction.Transactional;

/**
 * Lớp UserProfileService cung cấp các phương thức để quản lý thông tin hồ sơ
 * người dùng.
 * Lớp này triển khai giao diện IUserProfileService.
 */
@Service
@Transactional
public class UserProfileService extends AbService<Profile, UUID> implements IUserProfileService {
    @Autowired
    ProfileRepository profileRepository;

    /**
     * Phương thức này được sử dụng để tạo hồ sơ người dùng mới.
     *
     * @param createRequesrDto Đối tượng chứa thông tin để tạo hồ sơ.
     * @param user             Đối tượng người dùng liên quan đến hồ sơ.
     * @throws HttpException Ngoại lệ được ném khi người dùng đã có hồ sơ.
     */
    @Override
    public void createProfile(ProfileCreateRequestDto createRequesrDto, User user) throws HttpException {
        if (user.getProfile() != null) {
            throw new HttpException(HttpStatus.BAD_REQUEST, ProfileExceptionMessage.REGISTERED);
        }

        createRequesrDto.setUser(user);

        this.create(createRequesrDto);
    }
}