package com.example.cansachat.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.cansachat.Commond.HttpException;

/**
 * Dịch vụ gửi email.
 */
@Service
public class MailService {
    @Value("${spring.mail.username}")
    private String userEmail;

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * Gửi email đến địa chỉ email người nhận.
     *
     * @param recipientEmail Địa chỉ email người nhận.
     * @param subject        Tiêu đề của email.
     * @param messageMail    Nội dung của email.
     */
    public void send(String recipientEmail, String subject, String messageMail) throws HttpException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(userEmail);
        message.setTo(recipientEmail);
        message.setSubject(subject);
        message.setText(messageMail);
        try {
            javaMailSender.send(message);
        } catch (Exception e) {
            HttpException exception = new HttpException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
            throw exception;
        }
    }
}
