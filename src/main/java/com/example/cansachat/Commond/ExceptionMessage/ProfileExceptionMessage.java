package com.example.cansachat.Commond.ExceptionMessage;

/**
 * Lớp UserExceptionMessage chứa các thông báo lỗi liên quan đến thông tin người dùng.
 */
public class ProfileExceptionMessage {

    /**
     * Thông báo lỗi khi email đã được đăng ký.
     */
    public static final String REGISTERED = "Tài khoản đã được đăng ký thông tin";
}