package com.example.cansachat.Commond.ExceptionMessage;

/**
 * Lớp UserExceptionMessage chứa các thông báo lỗi liên quan đến tài khoản người dùng.
 */
public class UserExceptionMessage {

    /**
     * Thông báo lỗi khi tài khoản chưa được kích hoạt.
     */
    public static final String INACTIVE = "Tài khoản chưa được kích hoạt";

    /**
     * Thông báo lỗi khi email chưa được đăng ký.
     */
    public static final String UNREGISTER = "Email chưa được đăng ký";

    /**
     * Thông báo lỗi khi email đã được đăng ký.
     */
    public static final String REGISTERED = "Email đã được đăng ký";
}