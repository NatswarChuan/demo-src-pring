package com.example.cansachat.Commond.ExceptionMessage;

/**
 * Lớp chứa các thông báo lỗi liên quan đến OTP.
 */
public class OtpExceptionMessage {
    /**
     * Thông báo lỗi khi mã OTP không hợp lệ.
     */
    public static final String OTP_INVALID = "Mã OTP không hợp lệ";

    /**
     * Thông báo lỗi khi mã OTP đã hết hiệu lực.
     */
    public static final String OTP_EXPIRED = "Mã OTP đã hết hiệu lực";

    /**
     * Thông báo lỗi khi mã OTP đã hết hiệu lực hoặc không hợp lệ.
     */
    public static final String OTP_INVALID_OR_EXPIRED = "Mã OTP đã hết hiệu lực hoặc không hợp lệ";
}
