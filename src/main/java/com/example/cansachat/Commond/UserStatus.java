package com.example.cansachat.Commond;

/**
 * Định nghĩa các hằng số cho các loại trạng thái user.
 *
 * Các loại trạng thái user bao gồm:
 * - INACTIVE: Đã đăng ký và chưa kích hoạt.
 * - ACTIVE: Đã đăng ký và kích hoạt.
 * - PRIVACY: Đã đăng ký,đã kích hoạt và bật riêng tư.
 */
public class UserStatus {
    
    /**
     * Loại user đã đăng ký nhưng chưa kích hoạt.
     */
    public static final int  INACTIVE= 0;
   
    /**
     * Loại user đã đăng ký, đã kích hoạt nhưng không bật riêng tư.
     */
    public static final int ACTIVE = 1;

     /**
     * Loại user đã đăng ký, đã kích hoạt và bật riêng tư.
     */
    public static final int PRIVACY = 2;
}
