package com.example.cansachat.Interfaces.Services;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Entities.User;

/**
 * Giao diện định nghĩa các phương thức liên quan đến xác thực OTP.
 */
public interface IOtpService {

    /**
     * Kiểm tra OTP cho việc đăng nhập.
     *
     * @param email    Địa chỉ email của người dùng.
     * @param otpCode  Mã OTP được cung cấp.
     * @return         Người dùng nếu OTP hợp lệ.
     * @throws HttpException   Nếu có lỗi xảy ra trong quá trình xác thực OTP.
     */
    User checkOtpLogin(String email, String otpCode) throws HttpException;

    /**
     * Kiểm tra OTP cho việc đăng ký.
     *
     * @param email    Địa chỉ email của người dùng.
     * @param otpCode  Mã OTP được cung cấp.
     * @return         Người dùng nếu OTP hợp lệ.
     * @throws HttpException   Nếu có lỗi xảy ra trong quá trình xác thực OTP.
     */
    User checkOtpRegister(String email, String otpCode) throws HttpException;
}
