package com.example.cansachat.Interfaces.Services;

import com.example.cansachat.Commond.HttpException;

/**
 * Giao diện IUserService định nghĩa các phương thức liên quan đến việc quản lý người dùng.
 */
public interface IUserService {
    
    /**
     * Phương thức đăng nhập người dùng bằng email.
     *
     * @param email Địa chỉ email của người dùng.
     * @throws HttpException Ngoại lệ được ném ra khi có lỗi xảy ra trong quá trình đăng nhập.
     */
    void login(String email) throws HttpException;
    
    /**
     * Phương thức đăng nhập người dùng bằng email.
     *
     * @param email Địa chỉ email của người dùng.
     * @throws HttpException Ngoại lệ được ném ra khi có lỗi xảy ra trong quá trình đăng nhập.
     */
    void register(String email) throws HttpException;

    /**
     * Phương thức sinh mã OTP (One-Time Password).
     *
     * @return Mã OTP được sinh ra.
     */
    String generateOtpCode();
}
