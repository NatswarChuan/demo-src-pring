package com.example.cansachat.Interfaces.Services;

import java.util.UUID;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Dtos.Requests.ProfileCreateRequestDto;
import com.example.cansachat.Entities.Profile;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.IService;

public interface IUserProfileService extends IService<Profile,UUID> {
    void createProfile(ProfileCreateRequestDto createRequesrDto,User user)  throws HttpException;
}
