package com.example.cansachat.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.ResponseData;
import com.example.cansachat.Dtos.Requests.ProfileCreateRequestDto;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IUserProfileService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api")
public class UserProfileController extends AbController {
    @Autowired
    IUserProfileService profileService;

    @PutMapping(value = { "register-profile", "register-profile/" })
    public ResponseEntity<ResponseData<?>> registerProfile(
            @Valid @RequestBody ProfileCreateRequestDto profileCreateRequesrDto)  throws HttpException{
        logService.log(request.getRequestURI(), "call api");
        ResponseData<Object> response = new ResponseData<>();
        HttpStatus status = HttpStatus.CREATED;

        profileService.createProfile(profileCreateRequesrDto, (User) request.getAttribute("user"));
        
        response.setStatus(status);
        response.setMessage("Đăng ký thông tin tài khoản thành công");
        logService.success(request.getRequestURI(), "api response: " + response);
        return ResponseEntity.status(status).body(response);
    }
}
