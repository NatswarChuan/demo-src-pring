package com.example.cansachat.Controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.ResponseData;
import com.example.cansachat.Configs.JwtUtil;
import com.example.cansachat.Dtos.Requests.EmailRequest;
import com.example.cansachat.Dtos.Responses.AccessTokenResponse;
import com.example.cansachat.Dtos.Responses.UserResponseDto;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Services.MailService;

import jakarta.validation.Valid;

/**
 * Đây là một ví dụ về một Controller trong Spring Boot.
 *
 * @RestController: Đánh dấu đây là một Controller và các phương thức trong nó
 *                  sẽ trả về dữ liệu JSON.
 *                  @RequestMapping("/test"): Định nghĩa đường dẫn cơ sở "/test"
 *                  cho tất cả các phương thức trong Controller này.
 */
@RestController
@RequestMapping("/test")
public class DemoController extends AbController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private MailService mailService;

    /**
     * Phương thức này được sử dụng để tạo ra một token JWT.
     *
     * @return ResponseEntity chứa một đối tượng ResponseData bao gồm một đối tượng
     *         AccessTokenResponse.
     */
    @GetMapping("/generateToken")
    public ResponseEntity<ResponseData<AccessTokenResponse>> generateToken() {
        // Ghi log khi gọi API
        logService.log(request.getRequestURI(), "call api");

        // Tạo một token JWT bằng cách sử dụng JwtUtil
        String token = jwtUtil.generateToken(UUID.fromString("87f959fe-909b-44f6-8e96-09a88f6b2c42"));

        // Tạo đối tượng ResponseData với mã 200, thông báo thành công và đối tượng
        // AccessTokenResponse
        ResponseData<AccessTokenResponse> response = new ResponseData<>(HttpStatus.OK, "Success",
                new AccessTokenResponse(token));

        // Ghi log khi API trả về response
        logService.success(request.getRequestURI(), "api response: " + response);

        // Trả về response với mã HTTP 200 OK
        return ResponseEntity.ok(response);
    }

    /**
     * Phương thức này được sử dụng để xác thực token JWT và trả về thông tin người
     * dùng.
     *
     * @return ResponseEntity chứa một đối tượng ResponseData bao gồm một đối tượng
     *         UserResponseDto.
     */
    @GetMapping("/validateToken")
    public ResponseEntity<ResponseData<UserResponseDto>> validateToken() {
        // Ghi log khi gọi API
        logService.log(request.getRequestURI(), "call api");

        // Lấy đối tượng User từ thuộc tính "user" của request
        User user = (User) request.getAttribute("user");

        // Tạo đối tượng UserResponseDto từ đối tượng User
        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.toDto(user);

        // Tạo đối tượng ResponseData với mã 200, thông báo thành công và đối tượng
        // UserResponseDto
        ResponseData<UserResponseDto> response = new ResponseData<>(HttpStatus.OK, "Success", userResponseDto);

        // Ghi log khi API trả về response
        logService.success(request.getRequestURI(), "api response: " + response);

        // Trả về response với mã HTTP 200 OK
        return ResponseEntity.ok(response);
    }

    /**
     * Phương thức xử lý yêu cầu gửi email.
     *
     * @param emailRequest Đối tượng chứa thông tin email.
     * @return ResponseEntity chứa dữ liệu phản hồi.
     */
    @PostMapping("/sendEmail")
    public ResponseEntity<ResponseData<?>> sendEmail(@Valid @RequestBody EmailRequest emailRequest) throws HttpException {
        // Ghi log cho việc gọi API
        logService.log(request.getRequestURI(), "call api");

        // Khởi tạo đối tượng ResponseData để chứa dữ liệu phản hồi
        ResponseData<Object> response = new ResponseData<>();

        // Gửi email bằng dịch vụ mailService
        mailService.send(emailRequest.getEmail(), "test", "test");

        // Cập nhật thông tin phản hồi thành công
        response.setStatus(HttpStatus.OK);
        response.setMessage("success");

        // Ghi log cho việc phản hồi thành công của API
        logService.success(request.getRequestURI(), "api response: " + response);

        // Trả về phản hồi thành công với dữ liệu response
        return ResponseEntity.ok(response);

    }

}
