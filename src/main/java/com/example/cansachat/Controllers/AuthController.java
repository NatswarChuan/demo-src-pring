package com.example.cansachat.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.cansachat.Commond.HttpException;
import com.example.cansachat.Commond.ResponseData;
import com.example.cansachat.Commond.ExceptionMessage.OtpExceptionMessage;
import com.example.cansachat.Configs.JwtUtil;
import com.example.cansachat.Dtos.Requests.EmailRequest;
import com.example.cansachat.Dtos.Requests.OtpCheckRequest;
import com.example.cansachat.Dtos.Responses.AccessTokenResponse;
import com.example.cansachat.Entities.User;
import com.example.cansachat.Interfaces.Services.IOtpService;
import com.example.cansachat.Interfaces.Services.IUserService;

import jakarta.validation.Valid;

/**
 * Controller cho quản lý người dùng.
 */
@RestController
@RequestMapping("/api")
public class AuthController extends AbController {

    @Autowired
    IUserService userService;

    @Autowired
    IOtpService otpService;

    @Autowired
    private JwtUtil jwtUtil;

    /**
     * API để đăng nhập người dùng.
     * 
     * @param emailRequest Đối tượng chứa email của người dùng.
     * @return ResponseEntity chứa dữ liệu phản hồi của API.
     * @throws HttpException
     */
    @PostMapping({ "login/", "login" })
    public ResponseEntity<ResponseData<?>> login(@Valid @RequestBody EmailRequest emailRequest)
            throws HttpException {
        logService.log(request.getRequestURI(), "call api");
        ResponseData<Object> response = new ResponseData<>();
        String email = emailRequest.getEmail();
        HttpStatus status = HttpStatus.NO_CONTENT;
        userService.login(email);
        response.setStatus(status);
        response.setMessage("Đã gửi mã OTP");
        logService.success(request.getRequestURI(), "api response: " + response);
        return ResponseEntity.status(status).body(response);
    }

    /**
     * Kiểm tra OTP để đăng nhập.
     *
     * @param otpCheckRequest Đối tượng chứa thông tin OTP
     * @return ResponseEntity chứa dữ liệu AccessTokenResponse
     * @throws HttpException nếu xảy ra lỗi trong quá trình kiểm tra OTP
     */
    @PostMapping({ "login-otp", "login-otp/" })
    public ResponseEntity<ResponseData<AccessTokenResponse>> checkOtpLogin(
            @Valid @RequestBody OtpCheckRequest otpCheckRequest)
            throws HttpException {
        logService.log(request.getRequestURI(), "call api");
        ResponseData<AccessTokenResponse> response = new ResponseData<>();
        HttpStatus status = HttpStatus.OK;
        String email = otpCheckRequest.getEmail();
        String otpCode = otpCheckRequest.getOtp();

        User user = otpService.checkOtpLogin(email, otpCode);

        if (user == null) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_INVALID_OR_EXPIRED);
        }

        String token = jwtUtil.generateToken(user.getUserId());
        AccessTokenResponse data = new AccessTokenResponse(token);

        response.setData(data);
        response.setStatus(status);
        response.setMessage("Đăng nhập thànhd cômg");
        logService.success(request.getRequestURI(), "api response: " + response);

        return ResponseEntity.status(status).body(response);
    }

    /**
     * Đăng ký email và gửi mã OTP.
     *
     * @param emailRequest Đối tượng chứa thông tin email
     * @return ResponseEntity không chứa dữ liệu
     * @throws HttpException nếu xảy ra lỗi trong quá trình đăng ký
     */
    @PostMapping({ "register/", "register" })
    public ResponseEntity<ResponseData<?>> register(@Valid @RequestBody EmailRequest emailRequest)
            throws HttpException {
        logService.log(request.getRequestURI(), "call api");
        ResponseData<Object> response = new ResponseData<>();
        String email = emailRequest.getEmail();
        HttpStatus status = HttpStatus.NO_CONTENT;

        userService.register(email);

        response.setStatus(status);
        response.setMessage("Đã gửi mã OTP");
        logService.success(request.getRequestURI(), "api response: " + response);
        return ResponseEntity.status(status).body(response);
    }

    /**
     * Kiểm tra OTP để hoàn tất quá trình đăng ký.
     *
     * @param otpCheckRequest Đối tượng chứa thông tin OTP
     * @return ResponseEntity chứa dữ liệu AccessTokenResponse
     * @throws HttpException nếu xảy ra lỗi trong quá trình kiểm tra OTP
     */
    @PutMapping({ "register-otp", "register-otp/" })
    public ResponseEntity<ResponseData<AccessTokenResponse>> checkOtpRegister(
            @Valid @RequestBody OtpCheckRequest otpCheckRequest)
            throws HttpException {
        logService.log(request.getRequestURI(), "call api");
        ResponseData<AccessTokenResponse> response = new ResponseData<>();
        HttpStatus status = HttpStatus.CREATED;
        String email = otpCheckRequest.getEmail();
        String otpCode = otpCheckRequest.getOtp();

        User user = otpService.checkOtpRegister(email, otpCode);

        if (user == null) {
            throw new HttpException(HttpStatus.BAD_REQUEST, OtpExceptionMessage.OTP_INVALID_OR_EXPIRED);
        }

        String token = jwtUtil.generateToken(user.getUserId());
        AccessTokenResponse data = new AccessTokenResponse(token);

        response.setData(data);
        response.setStatus(status);
        response.setMessage("Đăng ký thànhd cômg");
        logService.success(request.getRequestURI(), "api response: " + response);

        return ResponseEntity.status(status).body(response);
    }
}
